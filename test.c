# include <stdio.h>
# include <stdlib.h>
# include <time.h>
# include <math.h>

double currTime = 0;
struct event
{
	/* data */
	double timestamp;// if this is needed----
    /*data for priority queue*/
    struct event *next;
};

void enqueue(struct event *ep);
struct event * dequeue();
struct event * header;
double randexp(double U);

void initialize(){
	header = malloc(sizeof(struct event));
	header->timestamp=0.00;
	header->next=NULL;
}

void holdOperation(){
	struct event *e;
	//printf("HEEE$EY\n");
	while(header->next !=NULL && currTime<14400.00)
	{//printf("HEEE$EY222222\n");
		e =dequeue();
		currTime = e->timestamp;
		free(e);
		e = malloc(sizeof(struct event));
		e->timestamp=currTime+randexp(1.00);
		enqueue(e);
	}
}

void enqueue(struct event *ep){//get this function checked. The while loop.-----
	struct event *temp;
	temp = header; //ponter to the header
	if(temp->next == NULL){
		temp->next=ep;
		ep->next=NULL;
	}
	else{
		while(temp->next !=NULL){
			if(temp->next->timestamp > ep->timestamp){
				ep->next=temp->next;
				temp->next=ep;
				break;
			}
			temp=temp->next;
		}
		if(temp->next==NULL){
			temp->next=ep;
			ep->next= NULL;
		}
	}
}

struct event * dequeue(){
	struct event *temp;
	temp = header->next;
	if (temp->next == NULL){
		/*if the last element in the queue*/
		header->next=NULL;
		return temp;
	}
	else {
		header->next=temp->next;
		return temp;
	}	
}

double urand(){
/*generates random numbers between [0,1)*/
	srand((time(NULL)));
	double randNum = (double)rand()/((double)RAND_MAX+1);
	return randNum;
}

double randexp(double U){
/*generates the random time interval with mean U*/	
	return ((-U)*log(1.0-urand()));
}

void eventGenerator(){
	struct event *e;
	e = malloc(sizeof(struct event));
	e->timestamp = randexp(1.00);
	enqueue(e);
}

int main(void)
{
		for(int N=10;N<1000;N+=10){
			initialize();
			currTime = 0;
			for(int i=1;i<N;i++){
			eventGenerator();
			}
		clock_t StartTime = clock();
		holdOperation();
		clock_t EndTime = clock();
	    clock_t TimeDifference = (EndTime-StartTime)/1000;
	    float avgTimeInSeconds = ((float) TimeDifference) /CLOCKS_PER_SEC;
	    printf("%d   %f\n",N,avgTimeInSeconds);
}
}