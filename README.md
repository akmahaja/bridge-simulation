# README #

== Description ==

This program allows you to simulate the bridgeCrossing events 
in two directions (Northbound and Southbound), where on bridge 
the cars can travel in one direction only.

== Installation ==

To install, compile the SimulationEngine.c and application.c 
files using a standard C compiler, such as

    gcc SimulationEngine.c
    gcc application.c
    gcc test.c
    gcc application_part2.c

== Execution ==

For the above command the executable file generated is a.out

    ./a.out

