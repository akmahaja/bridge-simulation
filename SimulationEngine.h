// Simulation Engine Interface

// Schedule an event with timestamp ts, event parameters *data, and callback function cb
void scheduleEvent (double ts, void (*f)(void), void *ed);

// Call this procedure to run the simulation
void runSimulation (void);

// This function returns the current simulation time
double current (void);