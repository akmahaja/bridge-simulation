#include <stdio.h>
#include <stdlib.h>
#include "SimulationEngine.c"
#include <math.h>
#include <time.h>

#define E 2.00
#define C 30.00

#define DB	1

int MAX_BRIDGE=0;
int NORTHBOUNDGROUP = 0;
int SOUTHBOUNDGROUP = 0;
int ON_BRIDGE=0;
double INTERARRIVAL_NORTHBOUND_MEAN=32.00;
double INTERARRIVAL_SOUTHBOUND_MEAN=32.00;
double INTERARRIVAL_MEAN=0.0;
double TotalWaitingTimeNorth = 0.0;
double TotalWaitingTimeSouth = 0.0;
int count=0;
int TotalVehiclesNorth=0;
int TotalVehiclesSouth=0;

typedef enum{
	EMPTY,
	NORTHBOUND,
	SOUTHBOUND
} BridegState;

typedef enum{
	NORTH,
	SOUTH
} Direction;

typedef enum{
	ARRIVAL,
	ENTER,
	DEPARTURE
} KindsofEvent;

struct Vehicle{
	int VehID;
	double startWaiting;
	double endWaiting;
	struct Vehicle *Next;
	Direction dir;
};

struct EventData
{
	KindsofEvent EventType;
	union{
		struct
		{
			int VehID;
			Direction dir;
		}Arrival;
		struct{
			struct Vehicle *vehicle;
			Direction dir;
		}Enter;
		struct{
			struct Vehicle *vehicle;
			Direction dir;
		}Departure;
	}EvParam;
};

BridegState BS = EMPTY;
struct Vehicle *NorthFront=NULL;
struct Vehicle *NorthEnd=NULL;
struct Vehicle *SouthFront=NULL;
struct Vehicle *SouthEnd=NULL;


double urand();
double randexp(double U);
void InsertFIFO(struct Vehicle *v);
struct Vehicle * DeleteFIFO();
void Arrival(struct EventData *e);
void Enter(struct EventData *e);
void Departure(struct EventData *e);



double urand(){
/*generates random numbers between [0,1)*/
	srand((time(NULL)));
	double randNum = (double)rand()/((double)RAND_MAX+1);
	return randNum;
}

double randexp(double U){
/*generates the random time interval with mean U*/	
	return ((-U)*log(1.0-urand()));
}

void InsertFIFO (struct Vehicle *v)
{
	v->Next = NULL;
	if (v->dir == NORTH){
		if (NorthFront==NULL) {
			// insert into empty queue
			NorthFront = v;
			NorthEnd = v;
		}
		else {
			// insert into non-empty queue
			NorthEnd->Next = v;
			NorthEnd = v;
		}
	}
	else if (v->dir == SOUTH){
		if (SouthFront==NULL) {
			// insert into empty queue
			SouthFront = v;
			SouthEnd = v;
		}
		else {
			// insert into non-empty queue
			SouthEnd->Next = v;
			SouthEnd = v;
		}		
	}
}

// Remove vehicle from FIFO queue, and return pointer to removed vehicle
// Error if the queue is empty
struct Vehicle *DeleteFIFO (Direction d)
{
	struct Vehicle *v;
	if(d == NORTH){
		if (NorthFront== NULL) {fprintf (stderr, "Unexpected empty queue\n"); exit(1);}
		v = NorthFront;		// save return value
		NorthFront = v->Next;
		if (NorthFront==NULL) NorthEnd=NULL;	
		return(v);	
	}
	else{
		if (SouthFront== NULL) {fprintf (stderr, "Unexpected empty queue\n"); exit(1);}
		v = SouthFront;		// save return value
		SouthFront = v->Next;
		if (SouthFront==NULL) SouthEnd=NULL;
		return (v);		
	}
}

void Arrival(struct EventData *e){
	/*schedule entering the bridge*/
	struct EventData *d;
	struct Vehicle *v;
	double ts;
	Direction direction;
	direction = e->EvParam.Arrival.dir;

	if (DB) {
		if(direction == NORTH)
			printf ("ArrivalEvent: time=%f, VehID=%d, Direction=North\n", current(), e->EvParam.Arrival.VehID);
		else if(direction == SOUTH)
			printf ("ArrivalEvent: time=%f, VehID=%d, Direction=South\n", current(), e->EvParam.Arrival.VehID);
	}
	//create vehicle
	if ((v=malloc (sizeof(struct Vehicle)))==NULL) {fprintf(stderr, "malloc error\n"); exit(1);}
	v->VehID = e->EvParam.Arrival.VehID;
	v->dir = e->EvParam.Arrival.dir;
	v->Next = NULL;


	if(direction == NORTH){
		NORTHBOUNDGROUP++;
		INTERARRIVAL_MEAN = INTERARRIVAL_NORTHBOUND_MEAN;
	}
	else if(direction == SOUTH){
		SOUTHBOUNDGROUP++;
		//printf("southbound group list%d\n",SOUTHBOUNDGROUP);
		INTERARRIVAL_MEAN = INTERARRIVAL_SOUTHBOUND_MEAN;
	}
	else{
		printf("Direction Error: The bride is North-South direction.\n");
	}

	//schedule next arrival
	if((d=malloc(sizeof(struct EventData)))==NULL) {fprintf(stderr, "malloc error\n"); exit(1);}
	d->EventType=ARRIVAL;
	d->EvParam.Arrival.VehID=++count;
	d->EvParam.Arrival.dir=direction;
	ts=current()+randexp(INTERARRIVAL_MEAN);
	scheduleEvent(ts, (void *) Arrival, d);

	if(BS == EMPTY){
		if(direction == NORTH){
			//printf("I arrived at %lf my vehicleId is %d and my direction is North\n",ts,v->VehID);
			BS=NORTHBOUND;
		}else if(direction == SOUTH){
		//printf("I arrived at %lf my vehicleId is %d and my direction is South\n",ts,v->VehID);
			BS=SOUTHBOUND;
		}

		if ((d=malloc (sizeof(struct EventData))) == NULL) {fprintf(stderr, "malloc error\n"); exit(1);}

		d->EventType=ENTER;
		d->EvParam.Enter.vehicle=v;
		d->EvParam.Enter.dir=direction;

		v->startWaiting=current();
		v->endWaiting=current();

		ts = current()+E;
		scheduleEvent(ts, (void *) Enter, d);
	}
	else{	

			v->startWaiting=current();
			if(direction == NORTH){
			//printf("I arrived at %lf my vehicleId is %d and my direction is North and my waiting starts @ %lf \n",current(),v->VehID,current());				
			}
			else{
			//printf("I arrived at %lf my vehicleId is %d and my direction is South and my waiting starts @ %lf\n",current(),v->VehID,current());
			}
			InsertFIFO(v);
	}

	free(e);
}

void Enter(struct EventData *e){

	struct EventData *d;
	struct Vehicle *v;
	double ts;
	Direction direction;
	//printf("I entered\n");
	direction=e->EvParam.Enter.dir;
	if (e->EventType != ENTER) {fprintf (stderr, "Unexpected event type\n"); exit(1);}
	if (DB) {
		if(direction == NORTH)
			printf ("EnteringEvent: time=%f, VehID=%d, Direction=North\n", current(), e->EvParam.Enter.vehicle->VehID);
		else if(direction == SOUTH)
			printf ("EnteringEvent: time=%f, VehID=%d, Direction=South\n", current(), e->EvParam.Enter.vehicle->VehID);
	}
	if(direction == NORTH){
		//printf("my vehicleId is %d and my direction is North and I am entering @ %lf \n",e->EvParam.Enter.vehicle->VehID,current());
		TotalVehiclesNorth++;
		NORTHBOUNDGROUP--;
		//printf("northbound group%d\n",NORTHBOUNDGROUP );
	}
	else if(direction == SOUTH){
		//printf("my vehicleId is %d and my direction is South and I am entering @ %lf \n",e->EvParam.Enter.vehicle->VehID,current());
		TotalVehiclesSouth++;
		SOUTHBOUNDGROUP--;
		//printf("southbound group %d\n",SOUTHBOUNDGROUP );
	}
	ON_BRIDGE++;

	if ((d=malloc (sizeof(struct EventData))) == NULL) {fprintf(stderr, "malloc error\n"); exit(1);}

	d->EventType=DEPARTURE;
	d->EvParam.Departure.vehicle=e->EvParam.Enter.vehicle;
	d->EvParam.Departure.dir=e->EvParam.Enter.dir;

	ts= current()+(C-E);
	scheduleEvent(ts, (void*) Departure, d);
	//printf("ASHWINI%lf\n",ts );
	if(ON_BRIDGE ==10){
		MAX_BRIDGE =1;
	}
	if(MAX_BRIDGE !=1){
		if(BS == NORTHBOUND){
			if(NORTHBOUNDGROUP >0){
				v = DeleteFIFO(direction);
				v->endWaiting=current();
				//printf("I arrived at %lf my vehicleId is %d and my direction is North and my waiting ends @ %lf \n",v->startWaiting,v->VehID,current());
				if ((d=malloc (sizeof(struct EventData))) == NULL) {fprintf(stderr, "malloc error\n"); exit(1);}

				//printf("I just entered %d\n",v->VehID);
				//printf("My waiting time ended at %lf\n",v->endWaiting);

				d->EventType=ENTER;
				d->EvParam.Enter.vehicle=v;
				d->EvParam.Enter.dir=direction;
				ts=current()+E;
				scheduleEvent(ts,(void*) Enter,d);			
			}
		}
		else if(BS == SOUTHBOUND){ 
			if(SOUTHBOUNDGROUP > 0){
				v = DeleteFIFO(direction);
				v->endWaiting=current();
				//printf("I arrived at %lf my vehicleId is %d and my direction is North and my waiting ends @ %lf \n",v->startWaiting,v->VehID,current());
				if ((d=malloc (sizeof(struct EventData))) == NULL) {fprintf(stderr, "malloc error\n"); exit(1);}

				//printf("I just entered %d\n",v->VehID);
				//printf("My waiting time ended at %lf\n",v->endWaiting);

				d->EventType=ENTER;
				d->EvParam.Enter.vehicle=v;
				d->EvParam.Enter.dir=direction;
				ts=current()+E;
				scheduleEvent(ts,(void*) Enter,d);				
			}
		}
	}
		/*else if(SOUTHBOUNDGROUP >0){
			BS = SOUTHBOUND;
			direction = SOUTH;
			v = DeleteFIFO(direction);
			v->endWaiting=current()+ON_BRIDGE*(C-E)+(SOUTHBOUNDGROUP-1)*E;
			if ((d=malloc (sizeof(struct EventData))) == NULL) {fprintf(stderr, "malloc error\n"); exit(1);}

			//printf("I just entered %d\n",v->VehID);
			//printf("My waiting time ended at %lf\n",v->endWaiting);

			//d->EventType=ENTER;
			//d->EvParam.Enter.vehicle=v;
			//d->EvParam.Enter.dir=direction;
			//printf("ONBRIDGE%d\n", ON_BRIDGE);
			//ts=current()+ON_BRIDGE*(C-E)+(SOUTHBOUNDGROUP*E);
			//scheduleEvent(ts,(void*) Enter,d);	
			}
		else{
			if(ON_BRIDGE == 0)
			BS = EMPTY;
		}
	}
	else if(BS == SOUTHBOUND){	
		if(SOUTHBOUNDGROUP >0){
			direction=SOUTH;
			v = DeleteFIFO(direction);
			v->endWaiting=current();
			//printf("I arrived at %lf my vehicleId is %d and my direction is South and my waiting ends @ %lf \n",v->startWaiting,v->VehID,current());
			if ((d=malloc (sizeof(struct EventData))) == NULL) {fprintf(stderr, "malloc error\n"); exit(1);}

			//printf("I just entered %d\n",v->VehID);
			//printf("My waiting time ended at %lf\n",v->endWaiting);

			d->EventType=ENTER;
			d->EvParam.Enter.vehicle=v;
			d->EvParam.Enter.dir=direction;
			ts=current()+E;
			scheduleEvent(ts,(void*) Enter,d);			
		}
		else if(NORTHBOUNDGROUP >0){
			BS = NORTHBOUND;
			direction = NORTH;
			v = DeleteFIFO(direction);
			v->endWaiting=current()+ON_BRIDGE*(C-E)+(NORTHBOUNDGROUP-1)*E;
			if ((d=malloc (sizeof(struct EventData))) == NULL) {fprintf(stderr, "malloc error\n"); exit(1);}

			//printf("I just entered %d\n",v->VehID);
			//printf("My waiting time ended at %lf\n",v->endWaiting);

			d->EventType=ENTER;
			d->EvParam.Enter.vehicle=v;
			d->EvParam.Enter.dir=direction;
			ts=current()+ON_BRIDGE*(C-E)+(NORTHBOUNDGROUP*E);
			scheduleEvent(ts,(void*) Enter,d);	
			}
		else{
			if(ON_BRIDGE == 0)
			BS = EMPTY;
		}
	}*/
	/*schedule departinng*/
	free(e);
}

void Departure(struct EventData *e){

	//printf("I m departing\n");
	Direction direction;
	struct Vehicle *v;
	struct EventData *d;
	double ts;
	if (e->EventType != DEPARTURE) {fprintf (stderr, "Unexpected event type\n"); exit(1);}
	if (DB){
		if(e->EvParam.Departure.dir == NORTH)
			printf ("DepartureEvent: time=%f, VehID=%d, Direction=North \n", current(), e->EvParam.Departure.vehicle->VehID);
		else if(e->EvParam.Departure.dir == SOUTH)
			printf ("DepartureEvent: time=%f, VehID=%d, Direction=South \n", current(), e->EvParam.Departure.vehicle->VehID);
	} 
	direction = e->EvParam.Departure.dir;
	ON_BRIDGE--;
	if(ON_BRIDGE ==0){
		MAX_BRIDGE =0;
		if(direction == NORTH && SOUTHBOUNDGROUP>0){
			BS = SOUTHBOUND;
			v = DeleteFIFO(SOUTH);
			v->endWaiting=current();
			if ((d=malloc (sizeof(struct EventData))) == NULL) {fprintf(stderr, "malloc error\n"); exit(1);}

			//printf("I just entered %d\n",v->VehID);
			//printf("My waiting time ended at %lf\n",v->endWaiting);
			d->EventType=ENTER;
			d->EvParam.Enter.vehicle=v;
			d->EvParam.Enter.dir=SOUTH;
			ts=current()+E;
			scheduleEvent(ts,(void*) Enter,d);	
		}
		else if(direction == SOUTH && NORTHBOUNDGROUP>0){
			BS = NORTHBOUND;
			v = DeleteFIFO(NORTH);
			v->endWaiting=current();
			if ((d=malloc (sizeof(struct EventData))) == NULL) {fprintf(stderr, "malloc error\n"); exit(1);}

			//printf("I just entered %d\n",v->VehID);
			//printf("My waiting time ended at %lf\n",v->endWaiting);
			d->EventType=ENTER;
			d->EvParam.Enter.vehicle=v;
			d->EvParam.Enter.dir=NORTH;
			ts=current()+E;
			scheduleEvent(ts,(void*) Enter,d);	
		}
		else{
			BS = EMPTY;
		}
	}
	if(direction == NORTH)
		TotalWaitingTimeNorth += (e->EvParam.Departure.vehicle->endWaiting - e->EvParam.Departure.vehicle->startWaiting);
	else if(direction == SOUTH)
		TotalWaitingTimeSouth += (e->EvParam.Departure.vehicle->endWaiting - e->EvParam.Departure.vehicle->startWaiting);
	/*departure event*/
	free(e->EvParam.Departure.vehicle);
	free(e);
}

int main (void)
{
	struct EventData *d;
	double ts;

	initialize();
	// initialize event list with first arrival
	if ((d=malloc (sizeof(struct EventData))) == NULL) {fprintf(stderr, "malloc error\n"); exit(1);}
	d->EventType = ARRIVAL;
	d->EvParam.Arrival.VehID = count;
	d->EvParam.Arrival.dir = NORTH;
	ts = randexp(INTERARRIVAL_NORTHBOUND_MEAN);
	scheduleEvent (ts, (void *) Arrival, d);

	if ((d=malloc (sizeof(struct EventData))) == NULL) {fprintf(stderr, "malloc error\n"); exit(1);}
	d->EventType = ARRIVAL;
	d->EvParam.Arrival.VehID = ++count;
	d->EvParam.Arrival.dir = SOUTH;
	ts = randexp(INTERARRIVAL_SOUTHBOUND_MEAN);
	scheduleEvent (ts, (void *) Arrival, d);

	printf ("Bridge Crossing Simulation: Regular version\n");
	runSimulation();

	printf ("Total waiting time for NorthBound Vehicles= %f\n", TotalWaitingTimeNorth);
	printf ("Total waiting time for SouthBound Vehicles= %f\n", TotalWaitingTimeSouth);
	printf ("Average waiting time for NorthBound Vehicles = %f\n", TotalWaitingTimeNorth / (double) TotalVehiclesNorth);
	printf ("Average waiting time for SouthBound Vehicles= %f\n", TotalWaitingTimeSouth / (double) TotalVehiclesSouth);
}