# include <stdio.h>
# include <stdlib.h>

double currTime = 0;
struct event
{
	/* data */
	double timestamp;// if this is needed----
	void * eData;
	void (*callback)(void *);
    /*data for priority queue*/
    struct event *next;
};
void enqueue(struct event *ep);
struct event * dequeue();
struct event * header;
double current();

void initialize(){
	header = malloc(sizeof(struct event));
	header->timestamp=0.00;
	header->next=NULL;
}

void scheduleEvent (double ts, void (*f)(void*), void *ed){
	struct event *e = malloc(sizeof(struct event));
	e->timestamp = ts;
	e->callback =f;
	e->eData =ed;
	enqueue(e);//confirm this function
}

void runSimulation(){
	struct event *e;
	while(header->next !=NULL && current()<14400.00)
	{
		e  = dequeue();
		//printf("ENGINE DATA%lf\n",e->timestamp );
		currTime = e->timestamp;
		e->callback(e->eData);
		free(e);
	}
}

double current(){
	return currTime;
}

void enqueue(struct event *ep){//get this function checked. The while loop.-----
	struct event *temp;
	temp = header; //ponter to the header
	if(temp->next == NULL){
		temp->next=ep;
		ep->next=NULL;
	}
	else{
		while(temp->next !=NULL){
			if(temp->next->timestamp > ep->timestamp){
				ep->next=temp->next;
				temp->next=ep;
				break;
			}
			temp=temp->next;
		}
		if(temp->next==NULL){
			temp->next=ep;
			ep->next= NULL;
		}
	}
}

struct event * dequeue(){
	struct event *temp;
	temp = header->next;
	if (temp->next == NULL){
		/*if the last element in the queue*/
		header->next=NULL;
		return temp;
	}
	else {
		header->next=temp->next;
		return temp;
	}	
}